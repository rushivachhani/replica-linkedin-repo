import React from 'react'

function VideoTestimonial(){
    return(
        <div class="container-fluid video_testimonial_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-5 video_testimonial_iframe_div">
						<iframe class="video_testimonial_iframe" src="https://www.youtube.com/embed/vyM4pLh7WW0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="Video Testimonial" />
					</div>
					<div class="col-lg-6 col-sm-7">
						<h2 class="video_testimonial_header">Check out Gayatri's story of finding a new job on LinkedIn</h2>
						<h3 class="video_testimonial_description">Check out Gayatri's story of finding a new job on LinkedIn</h3>
					</div>
				</div>
			</div>
		</div>
    )    
}
export default VideoTestimonial;