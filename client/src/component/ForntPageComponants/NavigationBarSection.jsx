import React from 'react';
import logo from '../../media/images/linkedin_logo.svg'

function NavigationBar(){
    return (
        <nav className="navbar navbar-light pt-3">
		    <div className="container-sm">
                <a href="link">
                    <img src={logo} alt="linkedin_logo" style={{width:'150px',height:'auto'}} className="m-0" />
                </a>
                <div className="d-flex">
                    <button type="button" className="btn btn-outline-secondary cust_nav_btn_1">Join with resume</button>
                    <button type="button" className="btn btn-outline-secondary cust_nav_btn_1 mx-2">Join now</button>
                    <span className="mt-2 text-muted fw-bold ms-3">|</span>
                    <button type="button" className="btn btn-outline-primary cust_nav_btn_2 mx-4">Sign in</button>
                </div>
			</div>
		</nav>        
    );
}

export default NavigationBar;