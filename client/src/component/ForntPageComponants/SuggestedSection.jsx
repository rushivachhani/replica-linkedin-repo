import React from 'react'

function SuggestedSection(){
    return(
        <div className="container-fluid suggested_section">
			<div className="container">
				<div className="row ">
					<div className="col-sm-5 ">
						<h2 className="find_your_job">Find open jobs and internships</h2>
					</div>
					<div className="col-sm  ">
						<h2 className="suggestred_searches py-2">SUGGESTED SEARCHES</h2>
						<button type="button" className="suggested_buttons ms-3 mb-3">Engineering</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Business Development</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Finance</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Administrative Assistant</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Retail Associate</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Customer Service</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Operations</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Information Technology</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Marketing</button>
						<button type="button" className="suggested_buttons ms-3 mb-3">Human Resources</button>
					</div>
				</div>
			</div>
		</div>
    )
}
 export default SuggestedSection;