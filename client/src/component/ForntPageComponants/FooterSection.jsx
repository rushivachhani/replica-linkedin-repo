import React from 'react'
import linkedin_logo from '../../media/images/linkedin_logo.svg'
function FooterSection(){
    return (
        <div className="container-fluid footer_container">
			<div className="container ">
				<div className="row">
					<div className="col-4 footer_column_1">
						<img src={linkedin_logo} alt="linkedin_logo" style={{width:'120px',height:'auto'}} className="m-0"/>
					</div>
					<div className="col-sm-4 col-lg-2 footer_column_1">
						<h5>General</h5>
						<ul className="list-unstyled text-small">
							<li>
							  <a href="#!">Link 1</a>
							</li>
							<li>
							  <a href="#!">Link 2</a>
							</li>
							<li>
							  <a href="#!">Link 3</a>
							</li>
							<li>
							  <a href="#!">Link 4</a>
							</li>
						  </ul>
					</div>
					<div className="col-sm-4 col-lg-2 footer_column_1">
						<h5>Browse LinkedIn</h5>
						<ul className="list-unstyled text-small">
							<li>
							  <a href="#!">Link 1</a>
							</li>
							<li>
							  <a href="#!">Link 2</a>
							</li>
							<li>
							  <a href="#!">Link 3</a>
							</li>
							<li>
							  <a href="#!">Link 4</a>
							</li>
						  </ul>
					</div>
					<div className="col-sm-4 col-lg-2 footer_column_1">
						<h5>Business Solutions</h5>
						<ul className="list-unstyled text-small">
							<li>
							  <a href="#!">Link 1</a>
							</li>
							<li>
							  <a href="#!">Link 2</a>
							</li>
							<li>
							  <a href="#!">Link 3</a>
							</li>
							<li>
							  <a href="#!">Link 4</a>
							</li>
							<li>
							  <a href="#!">Link 5</a>
							</li>
							<li>
							  <a href="#!">Link 6</a>
							</li>
							<li>
							  <a href="#!">Link 7</a>
							</li>
							<li>
							  <a href="#!">Link 8</a>
							</li>
						  </ul>
					</div>
					<div className="col-sm-4 col-lg-2  footer_column_1">
						<h5>Directories</h5>
						<ul className="list-unstyled text-small">
							<li>
							  <a href="#!">Link 1</a>
							</li>
							<li>
							  <a href="#!">Link 2</a>
							</li>
							<li>
							  <a href="#!">Link 3</a>
							</li>
							<li>
							  <a href="#!">Link 4</a>
							</li>
							<li>
							  <a href="#!">Link 5</a>
							</li>
							<li>
							  <a href="#!">Link 6</a>
							</li>
						  </ul>
					</div>
				</div>
			</div>
		</div>
    )
}
export default FooterSection;