import React from 'react'
import pubg from '../../media/images/pubg.png'
function GetStartedSection(){
    return(
        <div className="container-fluid get_started_container" style={{ backgroundImage: `url(${pubg})` }}>
			<div className="container ">
				<div className="row">
					<div className="get_started_header_div">
						<h1 className="get_started_header">Join your colleagues, classmates, and friends on LinkedIn.</h1>	
					</div>
				</div>
				<div className="row">
					<div className=" ">
						<button type="button" className="get_started_button">Get started</button>
					</div>
				</div>
			</div>
		</div>
    )
}
export default GetStartedSection;
