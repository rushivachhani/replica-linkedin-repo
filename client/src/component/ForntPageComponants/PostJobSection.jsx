import React from 'react'

function PostJobSection (){
    return(
        <div className="container-fluid post_job_container">
			<div className="container">
				<div className="row">
					<div className="col-5">
						<h1 className="post_job_message">Post your job and find the people you need</h1>
					</div>
					<div className="col-7 align-center">
						<button type="button" className="post_job_button">Post a job</button>
					</div>
				</div>
			</div>
		</div>
    )
}
export default PostJobSection;