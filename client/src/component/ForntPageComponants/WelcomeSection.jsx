import React from 'react'
import meeting_logo from '../../media/images/meeting.png'

function WelcomeSection(){
    return(
        <div className="container-sm pt-5 welcome-container" >
			<div className="row">
				<div className="col-sm ">
					<h1 className="welcome_line">Welcome to your <br /> professional <br /> community</h1>
				</div>
				<div className="col-sm-4  meeting_logo_position">
					<img src={meeting_logo} alt="Meeting" className="meeting_logo img-fluid m-0" />
				</div>
			</div>
			<div className="row mt-5">
				<div className="col-sm ">
					<div className="row mt-3">
						<div className="col">
							<button type="button" className="btn btn-light btn-lg py-3">Search for job 
								<svg width="2em" height="1.5em" viewBox="0 0 16 16" className="bi bi-chevron-compact-right align_right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	  								<path fill-rule="evenodd" d="M6.776 1.553a.5.5 0 0 1 .671.223l3 6a.5.5 0 0 1 0 .448l-3 6a.5.5 0 1 1-.894-.448L9.44 8 6.553 2.224a.5.5 0 0 1 .223-.671z"/>
								</svg>
							</button>
						</div>	
					</div>
					<div className="row mt-3">
						<div className="col">
							<button type="button" className="btn btn-light btn-lg py-3">Find a person you know 
								<svg width="2em" height="1.5em" viewBox="0 0 16 16" className="bi bi-chevron-compact-right align_right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	  								<path fill-rule="evenodd" d="M6.776 1.553a.5.5 0 0 1 .671.223l3 6a.5.5 0 0 1 0 .448l-3 6a.5.5 0 1 1-.894-.448L9.44 8 6.553 2.224a.5.5 0 0 1 .223-.671z"/>
								</svg>
							</button>
						</div>
					</div>
					<div className="row mt-3">
						<div className="col">
							<button type="button" className="btn btn-light btn-lg py-3">Learn a new skill 
								<svg width="2em" height="1.5em" viewBox="0 0 16 16" className="bi bi-chevron-compact-right align_right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	  								<path fill-rule="evenodd" d="M6.776 1.553a.5.5 0 0 1 .671.223l3 6a.5.5 0 0 1 0 .448l-3 6a.5.5 0 1 1-.894-.448L9.44 8 6.553 2.224a.5.5 0 0 1 .223-.671z"/>
								</svg>
							</button>
						</div>					
					</div>
				</div>
				<div className="col-sm-4 ">
					
				</div>
			</div>
		</div>
    )
}

export default WelcomeSection;