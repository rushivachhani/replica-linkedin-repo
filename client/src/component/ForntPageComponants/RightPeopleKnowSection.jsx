import React from 'react'
import right_people_know_image from '../../media/images/right_people_know_image.png'

function RightPeopleKnow(){
    return(
        <div className="container-fluid right_people_know_container">
			<div className="container">
				<div className="row">
					<div className="col-sm-5">
						<h1 className="right_people_know_header">Let the right people know you’re open to work</h1>
						<span className="right_people_know_description" >With the Open To Work feature, you can privately tell recruiters or publicly share with the LinkedIn community that youre looking for new job opportunities</span>
					</div>
					<div className="col-sm-7 text-center">
						<img src={right_people_know_image} alt="Let Right People Know You" className="right_people_know_image img-fluid m-0"/>
					</div>
				</div>
			</div>
		</div>
    )
}
export default RightPeopleKnow;