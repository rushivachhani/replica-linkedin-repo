import React from 'react'
import linkedin_logo from '../../media/images/linkedin_logo.svg'
function OfficialFooterSection(){
    return(
        <div className="container-fluid ">
			<div className="container text-center">
				
					<ul className="official_footer_ul">
                        <li>
                            <img src={linkedin_logo} alt="linkedin_logo" style={{width:'70px',height:'auto'}} className="m-0"/>
                        </li>
                        <li>
                            © 2020
                        </li>
                        <li>
                            General 
                        </li>
                        <li>
                            About
                        </li>
                        <li>
                            Accessibility
                        </li>
                        <li>
                            User Agreement
                        </li>
                        <li>
                            Privacy Policy
                        </li>
                        <li>
                            Cookie Policy
                        </li>
                        <li>
                            Copyright Policy
                        </li>
                        <li>
                            Brand Policy
                        </li>
                        <li>
                            Guest Controls
                        </li>
                        <li>
                            Community Guidelines
                        </li>
                        <li>
                            Lanugage
                        </li>
                    </ul>
			</div>
		</div>
    )
}
export default OfficialFooterSection;