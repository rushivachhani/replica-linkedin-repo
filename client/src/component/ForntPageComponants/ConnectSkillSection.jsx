import React from 'react'
import connect_with_people_image from '../../media/images/connect_with_people_image.svg'
import learn_skills_image from '../../media/images/learn_skills_image.svg'

 function ConnectSkill(){
     return(
        <div class="container-fluid connect_people_learn_skill_container">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 connect_with_people_column">
                        <div class="row mb-4">
                            <div class="col text-center">
                                <img src={connect_with_people_image} alt="Connect with people" class="connect_with_people_image img-fluid m-0"/>		
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h1 class="connect_with_people_header">Connect with people who can help</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="button" class="connect_with_people_button">Find people you know</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 learn_skill_column">
                        <div class="row mb-4">
                            <div class="col text-center">
                                <img src={learn_skills_image} alt="Learn new skill" class="learn_skills_image img-fluid m-0"/>		
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h1 class="learn_skills_header">Learn the skills that can help you now</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="button" class="learn_skill_button">Choose a topic to learn about</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     )
 }
 export default ConnectSkill;