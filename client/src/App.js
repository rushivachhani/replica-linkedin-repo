import './App.css';
import NaviagtionBar from './component/ForntPageComponants/NavigationBarSection'
import WelcomeSection from './component/ForntPageComponants/WelcomeSection'
import SuggestedSection from './component/ForntPageComponants/SuggestedSection'
import PostJobSection from './component/ForntPageComponants/PostJobSection'
import RightPeopleKnow from './component/ForntPageComponants/RightPeopleKnowSection'
import ConnectSkill from './component/ForntPageComponants/ConnectSkillSection'
import VideoTestimonial from './component/ForntPageComponants/VideoTestimonialSection'
import GetStartedSection from './component/ForntPageComponants/GetStartedSection'
import FooterSection from './component/ForntPageComponants/FooterSection'
import OfficialFooterSection from './component/ForntPageComponants/OfficialFooterSection'
function App() {
  return (
    <div className="">
      <NaviagtionBar></NaviagtionBar>

      <WelcomeSection></WelcomeSection>

      <SuggestedSection></SuggestedSection>

      <PostJobSection></PostJobSection>

      <RightPeopleKnow></RightPeopleKnow>
      
      <ConnectSkill></ConnectSkill>
      
      <VideoTestimonial></VideoTestimonial>

      <GetStartedSection></GetStartedSection>

      <FooterSection></FooterSection>
      
      <OfficialFooterSection></OfficialFooterSection>
    </div>
  );
}

export default App;
